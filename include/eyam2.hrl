%%%-------------------------------------------------------------------
%%% @author johnzeng
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. 一月 2017 上午11:13
%%%-------------------------------------------------------------------

%% define for unit test
-define(TEST_APPKEY, <<"58072d1fd69873332db470a6">>).
-define(TEST_SECKEY, <<"sec-YCQmkjDLLxfkSfrd90sUCmngPDlAhBODGCp3fFnLy2n5fPXH">>).
-define(TEST_APPKEY2, <<"581953df1a9bd33b8700e203">>).
-define(TEST_SECKEY2, <<"123456">>).


%% define for eyam_handler
-define(CMD_GRANT, <<"yam_grant">>).
-define(CMD_AUDIT, <<"yam_audit">>).
-define(CMD_SET, <<"yam_set">>).
-define(CMD_GET, <<"yam_get">>).
-define(YAM_ETCD_PREFIX, "/config/yam/").

% opt should be #grant if it's called for grant or audit
% opt should be trigger(0/1) if it's called for yam_set
-record(base, {appkey, cmd, seckey, opt}).
-record(grant,{topic, alias, token, uid, w, r, p, ttl}).
-record(grant_key_args, {appkey, topic, uid, token, r, w, ttl}).
-record(etcd_kv, {key, value, ttl, timeout}).
