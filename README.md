## eyam2
Erlang Yunba Access Manager 2。云巴权限管理第二版。

整体设计文档参考 [design.md](doc/design.md)

初期设计文档 [access-manager-design](https://bitbucket.org/msgbus/mbus_docs/wiki/access-manager-design)

### how to run

如果本地环境的redis以及mongodb都使用默认接口，则直接拷贝`rel/files/app.config.example`为`rel/files/app.config`，以及`rel/files/vm.args.example`拷贝为`rel/files/vm.args`，即可本地启动。

默认占用8099端口为http服务端口。可以通过配置`app.config`的`eyam2/port`选项进行修改。

`make foreground` 应该可以启动为前台模式，一般开发可以使用这个。

其他开关包括

```
make run   	    #后台模式启动。	
make kill      #停止后台模式启动的应用。
make console   #控制台模式启动。
```

### to do test

如果想运行测试，需要在本地的mongodb准备如下内容：

1,27017端口的mongodb
2,使用drywall数据库
3,`db.apps.find()`至少返回以下内容

```
> db.apps.find()
{ "_id" : ObjectId("58072d1fd69873332db470a6"), "isYamEnable" : 1, "seckey" : "sec-YCQmkjDLLxfkSfrd90sUCmngPDlAhBODGCp3fFnLy2n5fPXH" }
{ "_id" : ObjectId("581953df1a9bd33b8700e203"), "seckey" : "123456" }
```

然后执行`make test`即可

### 配置文件

如果本地不是使用的redis和mongodb的默认端口，则需要对应地配置以下路径的内容。

```
eyam2/redis_yam
eyam2/mongo_yam
```

redis用于存储与权限控制相关的内容。mongodb只用于校验appkey和seckey。默认的database名称为drywall。

### 接口文档

RESTful API : [RESTfulAPI.md](doc/RESTfulAPI.md)

RPC API [rpc.md](doc/rpc.md)

### benchmark

[benchmark.md](doc/benchmark.md)


