-module(rpc_tests).

- compile(export_all).

all() ->
    [
     test_remote_check_permission
    ].

test_remote_check_permission(_) ->
    eyam2_util:redis_set(<< "K_testoken">>, 1, undefined),
    eyam2_util:redis_set(<< "C_testappId_testch">>, <<"2">>, undefined),

    false = eyam2_remote:check_permission(<<"testappId3">>, <<"testch">>, <<"testtoken">>, <<"testuid">>, <<"r">>),
    true = eyam2_remote:check_permission(<<"testappId">>, <<"testch">>, <<"testtoken">>, <<"testuid">>, <<"r">>),
    false = eyam2_remote:check_permission(<<"testappId">>, <<"testch2">>, <<"testtoken">>, <<"testuid">>, <<"w">>).

