-module(handler_tests).

- compile(export_all).

all() ->
    [
        test_yam_turn_on_yam,
        test_yam_get_yam_trigger_on,
        test_yam_turn_off_yam,
        test_yam_get_yam_trigger_off,
        test_yam_grant_should_get_not_permit_for_off_appkey,
        test_yam_grant_token_and_uid_should_get_invalid_param,
        test_yam_grant_by_get,
        test_yam_grant_by_get0,
        test_yam_grant_by_get1,
        test_yam_grant_by_get2,
        test_yam_grant_by_post,
        test_yam_grant_by_post_with_unexists_token,
        test_yam_audit_by_get0,
        test_yam_audit_by_get1,
        test_yam_audit_by_get2,
        test_yam_audit_by_get3,
        test_yam_audit_by_post,
        test_yam_grant_alias_by_get,
        test_yam_grant_alias_by_post,
        test_yam_audit_alias_by_get,
        test_yam_audit_alias_by_post
    ].

-include("eyam2.hrl").

test_yam_turn_on_yam(Config) ->
    Url = generate_rest_url_for_get(
            ?TEST_APPKEY2,
            ?TEST_SECKEY2,
            [
             {"method", "yam_set"},
             {"trigger", "1"}
            ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            try check_body(Body) of
                {failed, _} ->{failed, "fail to turn on"};
                {ok, _} -> ok
            catch
                badmatch -> {failed, "nothing match my try"}
            end;
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_get_yam_trigger_on(Config) ->
    Url = generate_rest_url_for_get(
            ?TEST_APPKEY2,
            ?TEST_SECKEY2,
            [
             {"method", "yam_get"}
            ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            try check_body(Body) of
                {failed, _} ->{failed, "fail to get trigger"};
                {ok, Props} ->
                    1 = proplists:get_value(<<"trigger">>, Props)
            catch
                badmatch -> {failed, "nothing match my try"}
            end;
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_turn_off_yam(Config) ->
    Url = generate_rest_url_for_get(
            ?TEST_APPKEY2,
            ?TEST_SECKEY2,
            [
             {"method", "yam_set"},
             {"trigger", "0"}
            ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            try check_body(Body) of
                {failed, _} ->{failed, "fail to turn off"};
                {ok, _} -> ok
            catch
                badmatch -> {failed, "nothing match my try"}
            end;
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_get_yam_trigger_off(Config) ->
    Url = generate_rest_url_for_get(
            ?TEST_APPKEY2,
            ?TEST_SECKEY2,
            [
             {"method", "yam_get"}
            ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            try check_body(Body) of
                {failed, _} ->{failed, "fail to get trigger"};
                {ok, Props} ->
                    0 = proplists:get_value(<<"trigger">>, Props)
            catch
                badmatch -> {failed, "nothing match my try"}
            end;
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_should_get_not_permit_for_off_appkey(Config) ->
    Token = proplists:get_value(token, Config),
    Url = generate_rest_url_for_get(
            ?TEST_APPKEY2,
            ?TEST_SECKEY2,
            [
             {"method", "yam_grant"},
             {"alias", "testch"},
             {"token", binary_to_list(Token)},
             {"ttl", "0"},
             {"w", "1"},
             {"r", "0"}
            ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            try check_body(Body) of
                {ok, _} ->{failed, "should not permit for not on trigger"};
                {failed, _} -> ok
            catch
                badmatch -> {ok, good}
            end;
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_token_and_uid_should_get_invalid_param(Config) ->
    Token = proplists:get_value(token, Config),
    Url = generate_rest_url_for_get(
            ?TEST_APPKEY,
            ?TEST_SECKEY,
            [
             {"method", "yam_grant"},
             {"channel", "testch1"},
             {"token", binary_to_list(Token)},
             {"uid", "test_uid"},
             {"ttl", "0"},
             {"w", "1"},
             {"r", "0"}
            ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            try check_body(Body) of
                {ok, _} ->{failed, "should not pass the uid and token togather"};
                {failed, _} -> ok
            catch
                badmatch -> {ok, good}
            end;
        {error, Reason} -> 
            ok
    end.


test_yam_grant_alias_by_get(Config) ->
    Token = proplists:get_value(token, Config),
    Url = generate_rest_url_for_get([
        {"method", "yam_grant"},
        {"alias", "testch"},
        {"token", binary_to_list(Token)},
        {"ttl", "0"},
        {"w", "1"},
        {"r", "0"}
    ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            check_body(Body);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_alias_by_post(Config) ->
    Token = proplists:get_value(token, Config),
    ct:log("the token is ~p", [Token]),
    Content = generate_content_for_post([
        {<<"method">>, <<"yam_grant">>},
        {<<"alias">>, <<"testch1">>},
        {<<"token">>, Token},
        {<<"ttl">>, <<"0">>},
        {<<"w">>, <<"1">>},
        {<<"r">>, <<"1">>}
    ]),

    case http_post(get_url(),Content) of
        {ok, Body} -> 
            check_body(Body);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_audit_alias_by_get(Config) ->
    Token = proplists:get_value(token, Config),
    Url = generate_rest_url_for_get([
        {"method", "yam_audit"},
        {"alias", "testch"},
        {"token", binary_to_list(Token)}
    ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            {ok, Props} = check_body(Body),
            ct:log("props is : ~p", [Props]),
            1 = proplists:get_value(<<"w">>, Props),
            0 = proplists:get_value(<<"r">>, Props);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_audit_alias_by_post(Config) ->
    Token = proplists:get_value(token, Config),
    Content = generate_content_for_post([
        {<<"method">>, <<"yam_audit">>},
        {<<"alias">>, <<"testch1">>},
        {<<"token">>, Token}
    ]),

    case http_post(get_url(),Content) of
        {ok, Body} -> 
            {ok, Props} = check_body(Body),
            1 = proplists:get_value(<<"w">>, Props),
            1 = proplists:get_value(<<"r">>, Props);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_by_get(_) ->
    Url = generate_rest_url_for_get([
        {"method", "yam_grant"},
        {"topic", "testch"},
        {"token", ""},
        {"ttl", "0"},
        {"w", "1"},
        {"r", "0"},
        {"p", "1"}
    ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            check_body(Body);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_by_get0(_) ->
    Url = generate_rest_url_for_get([
        {"method", "yam_grant"},
        {"topic", "testch"},
        {"ttl", "0"},
        {"w", "1"},
        {"r", "0"},
        {"p", "1"}
    ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            check_body(Body);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_by_get1(_) ->
    Url = generate_rest_url_for_get([
        {"method", "yam_grant"},
        {"ttl", "0"},
        {"w", "1"},
        {"r", "0"}
    ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            check_body(Body);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_by_get2(_) ->
    Url = generate_rest_url_for_get([
        {"method", "yam_grant"},
        {"topic", "testch1"},
        {"uid", "testuid"},
        {"ttl", "0"},
        {"w", "1"},
        {"r", "0"}
    ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            check_body(Body);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_grant_by_post_with_unexists_token(Config) ->
    Content = generate_content_for_post([
        {<<"method">>, <<"yam_grant">>},
        {<<"topic">>, <<"testch1">>},
        {<<"token">>, <<"unexist_token">>},
        {<<"ttl">>, <<"0">>},
        {<<"w">>, <<"1">>},
        {<<"r">>, <<"1">>},
        {<<"p">>, <<"1">>}
    ]),

    case http_post(get_url(),Content) of
        {ok, Body} -> 
            case check_body(Body) of 
                {ok, _} ->{failed, "should not pass the unexist token to grant"};
                {failed, _} -> ok
            end;
        {error, Reason} -> 
            ok
    end.

test_yam_grant_by_post(Config) ->
    Token = proplists:get_value(token, Config),
    ct:log("the token is ~p", [Token]),
    Content = generate_content_for_post([
        {<<"method">>, <<"yam_grant">>},
        {<<"topic">>, <<"testch1">>},
        {<<"token">>, Token},
        {<<"ttl">>, <<"0">>},
        {<<"w">>, <<"1">>},
        {<<"r">>, <<"1">>},
        {<<"p">>, <<"1">>}
    ]),

    case http_post(get_url(),Content) of
        {ok, Body} -> 
            check_body(Body);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_audit_by_get0(Config) ->
    Token = proplists:get_value(token, Config),
    Url = generate_rest_url_for_get([
        {"method", "yam_audit"},
        {"topic", "testch"},
        {"token", binary_to_list(Token)}
    ]),
    ct:log("the url is :~p", [Url]),

    case http_get(Url) of
        {ok, Body} -> 
            {ok, Props} = check_body(Body),
            ct:log("props is : ~p", [Props]),
            1 = proplists:get_value(<<"w">>, Props),
            0 = proplists:get_value(<<"r">>, Props),
            1 = proplists:get_value(<<"p">>, Props);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_audit_by_get1(Config) ->
    Url0 = generate_rest_url_for_get([
        {"method", "yam_audit"},
        {"topic", "testch"}
    ]),

    case http_get(Url0) of
        {ok, Body} -> 
            {ok, Props} = check_body(Body),
            ct:log("props is : ~p", [Props]),
            1 = proplists:get_value(<<"w">>, Props),
            0 = proplists:get_value(<<"r">>, Props),
            1 = proplists:get_value(<<"p">>, Props);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_audit_by_get2(Config) ->
    Url1 = generate_rest_url_for_get([
        {"method", "yam_audit"}
    ]),

    case http_get(Url1) of
        {ok, Body} -> 
            {ok, Props} = check_body(Body),
            ct:log("props is : ~p", [Props]),
            1 = proplists:get_value(<<"w">>, Props),
            0 = proplists:get_value(<<"r">>, Props),
            0 = proplists:get_value(<<"p">>, Props);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_audit_by_get3(Config) ->
    Url1 = generate_rest_url_for_get([
        {"method", "yam_audit"},
        {"uid", "testuid"},
        {"topic", "testch1"}
    ]),

    case http_get(Url1) of
        {ok, Body} -> 
            {ok, Props} = check_body(Body),
            ct:log("props is : ~p", [Props]),
            1 = proplists:get_value(<<"w">>, Props),
            0 = proplists:get_value(<<"r">>, Props),
            0 = proplists:get_value(<<"p">>, Props);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

test_yam_audit_by_post(Config) ->
    Token = proplists:get_value(token, Config),
    Content = generate_content_for_post([
        {<<"method">>, <<"yam_audit">>},
        {<<"topic">>, <<"testch1">>},
        {<<"token">>, Token}
    ]),

    case http_post(get_url(),Content) of
        {ok, Body} -> 
            {ok, Props} = check_body(Body),
            1 = proplists:get_value(<<"w">>, Props),
            1 = proplists:get_value(<<"r">>, Props),
            1 = proplists:get_value(<<"p">>, Props);
        {error, Reason} -> 
            ct:log("error: ~w" , [Reason]),
            {failed, Reason}
    end.

generate_rest_url_for_get(AppKey, SecKey, Params) ->
    GetParams = string:join(lists:map(fun ({Key, Value}) ->
        lists:flatten(io_lib:format("~s=~s", [Key, Value]))
    end, Params), "&"),

    HTTPPort = application:get_env(eyam2, port, 8099),

    PortStr = integer_to_list(HTTPPort),

    "http://localhost:" ++ PortStr ++ "/?appkey=" ++ binary_to_list(AppKey )++
    "&seckey=" ++ binary_to_list(SecKey) ++ "&" ++ GetParams.

generate_rest_url_for_get(Params) ->
    generate_rest_url_for_get(?TEST_APPKEY, ?TEST_SECKEY, Params).

http_get(URL) ->
    meck_init(),
    Ret =
    try ibrowse:send_req(URL,[],get) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" ->
                    {ok, Body};
                _Other ->
                    {error, Body}
                end;
        {error, connection_closing} ->
            {error, connection_closing};
        {error, REASON} ->
            {error, REASON}
    catch
        Type:Error ->
            {error, <<"catch error">>}
    end,
    meck_end(),
    Ret.

check_body(Body) ->
    ct:log("the body is: ~w", [Body]),
    case jiffy:decode(Body) of
        {Props} ->
            ct:log("the Props are ~w" , [Props]),
            Status = proplists:get_value( <<"status">>, Props),
            if 0 == Status  -> {ok, Props};
               true -> {failed, Props}
            end;
        _ -> {failed, Body}
    end.

generate_content_for_post(Params) ->
    generate_content_for_post(?TEST_APPKEY, ?TEST_SECKEY, Params).

generate_content_for_post(Appkey, Seckey, Params) ->
  jiffy:encode({[
    {<<"appkey">>, Appkey},
    {<<"seckey">>, Seckey}
    | Params
  ]}).


http_post(URL,Content) ->
    meck_init(),
    Ret = 
    try ibrowse:send_req(URL,[{"Content-type", "application/json"}],post, Content, [], 1000) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" ->
                    {ok, Body};
                _Other ->
                    {error, Body}
            end;
        {error, connection_closing} ->
            {error, connection_closing};
        {error, REASON} ->
            {error, <<"http failed">>}
    catch
        Type:Error ->
            {error, <<"http failed">>}
    end,
    meck_end(),
    Ret.

get_url() ->
    Http_port = application:get_env(eyam2, port, 8099),
    "http://localhost:" ++ integer_to_list(Http_port ) ++ "/".

meck_init() ->
    meck:new(eyam2_logic, [passthrough]),
    meck:expect(eyam2_logic, get_yam_trigger_from_redis, fun meck_yam_trigger/1).

meck_end() ->
    meck:unload().

meck_yam_trigger(Key) ->
    case Key of
        ?TEST_APPKEY ->
            {ok, 1};
        ?TEST_APPKEY2 ->
            {ok, 0};
        _ ->
            {ok, 0}
    end.
