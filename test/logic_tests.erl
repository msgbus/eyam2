-module(logic_tests).

- compile(export_all).

all()->
    [
     test_get_grant_key,
     test_binary_rw_to_interger,
     test_check_permission,
     test_token_exists,
     test_create_new_token,
     test_check_seckey,
     test_check_trigger
    ].

test_get_grant_key(_) ->
    <<"A_app">> = eyam2_logic:get_grant_key(<<"app">>, undefined, undefined, undefined),
    <<"C_app_ch">> = eyam2_logic:get_grant_key(<<"app">>, <<"ch">>, undefined, undefined),
    <<"I_app_ch_cde">> = eyam2_logic:get_grant_key(<<"app">>, <<"ch">>, undefined, <<"cde">>),
    <<"U_app_ch_abc">> = eyam2_logic:get_grant_key(<<"app">>, <<"ch">>, <<"abc">>, undefined).

test_binary_rw_to_interger(_) ->
    0 = eyam2_logic:rw_to_integer(<<"0">>, <<"0">>),
    1 = eyam2_logic:rw_to_integer(<<"0">>, <<"1">>),
    2 = eyam2_logic:rw_to_integer(<<"1">>, <<"0">>),
    3 = eyam2_logic:rw_to_integer(<<"1">>, <<"1">>).

test_check_permission(_) ->
    eyam2_util:redis_set(<< "A_testappId2">>, <<"1">>, undefined),
    eyam2_util:redis_set(<< "C_testappId_testch">>, <<"2">>, undefined),
    eyam2_util:redis_set(<< "U_testappId_testch2_testtoken">>, <<"2">>, undefined),
    eyam2_util:redis_set(<< "K_testtoken">>, <<"1">>, undefined),
    {ok, {0, 1}} = eyam2_logic:check_permission(<<"testappId2">>, undefined,undefined, undefined),
    {ok, {1, 0}} = eyam2_logic:check_permission(<<"testappId">>, <<"testch">>,undefined, undefined),
    {ok, {1, 0}} = eyam2_logic:check_permission(<<"testappId">>, <<"testch2">>,<<"testtoken">>, undefined),
    {ok, {0, 0}} = eyam2_logic:check_permission(<<"testappId3">>, undefined, undefined, undefined).

test_token_exists(_) ->
    eyam2_util:redis_set(<< "K_testoken">>, 1, undefined),
    true = eyam2_logic:token_exists(<<"testoken">>),
    false = eyam2_logic:token_exists(<<"unexisted_token">>).

test_create_new_token(_) ->
    Token = eyam2_logic:create_new_token(<<"a">>, <<"b">>, <<"1">>),
    true = is_binary(Token).

-include("eyam2.hrl").

test_check_seckey(_) ->
    true = eyam2_logic:check_secret_key(?TEST_APPKEY, ?TEST_SECKEY),
    true = eyam2_logic:check_secret_key(?TEST_APPKEY2, ?TEST_SECKEY2),
    false = eyam2_logic:check_secret_key(?TEST_APPKEY, ?TEST_SECKEY2),
    false = eyam2_logic:check_secret_key(?TEST_APPKEY2, ?TEST_SECKEY).

test_check_trigger(_) ->
    meck:new(eyam2_logic, [passthrough]),
    meck:expect(eyam2_logic, get_yam_state_from_etcd, fun meck_yam_state_from_etcd/1),

    {ok, 1} = eyam2_logic:get_yam_trigger_from_redis(?TEST_APPKEY),
    {ok, 0} = eyam2_logic:get_yam_trigger_from_redis(?TEST_APPKEY2),

    %% should test again for cache
    {ok, 1} = eyam2_logic:get_yam_trigger_from_redis(?TEST_APPKEY),
    {ok, 0} = eyam2_logic:get_yam_trigger_from_redis(?TEST_APPKEY2),
    meck:unload().

%% etcd will return in true false
meck_yam_state_from_etcd(Key) ->
    case Key of
        ?TEST_APPKEY ->
            {ok, true};
        ?TEST_APPKEY2 ->
            {ok, false};
        _ ->
            {ok, false}
    end.

