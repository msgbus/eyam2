- module(eyam_SUITE).

- compile(export_all).

all() ->
    [
     {group, handler},
     {group, logic},
     {group, rpc}
    ].

init_per_suite(Config) ->
    {ok, _} = application:ensure_all_started(ibrowse),
    {ok, _} = application:ensure_all_started(eyam2),
    {ok, _} = application:ensure_all_started(lager),
    Config.

groups() ->
    [
     {handler, [], [{handler_tests, all}]},
     {logic , [], [{logic_tests, all}]},
     {rpc, [], [{rpc_tests, all}]}
    ].

init_per_group(handler, Config) ->
    eredis_pool:q(eyam, ["FLUSHALL"]),
    {ok, Props} = handler_tests:test_yam_grant_by_get(ignore),
    Token = proplists:get_value(<<"token">>, Props),
    [{token, Token} | Config];
init_per_group(_ , Config) ->
    eredis_pool:q(eyam, ["FLUSHALL"]),
    Config.

end_per_group(_, Config) ->
    %do nothing
    Config.

