.PHONY: all deps clean release test

all: compile

compile: deps
	./rebar -j8 compile

deps:
	./rebar -j8 get-deps

clean:
	./rebar -j8 clean

relclean:
	rm -rf rel/eyam2

generate: compile
	cd rel && .././rebar -j8 generate

run: generate
	./rel/eyam2/bin/eyam2 start

kill: generate
	./rel/eyam2/bin/eyam2 stop

console: generate
	./rel/eyam2/bin/eyam2 console

foreground: generate
	./rel/eyam2/bin/eyam2 foreground

erl: compile
	erl -pa ebin/ -pa deps/*/ebin/ -s eyam2

test:
	./rebar compile ct suite=eyam skip_deps=true
