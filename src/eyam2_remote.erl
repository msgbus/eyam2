-module(eyam2_remote).

-export([check_permission/5, check_appkey_trigger/1]).

%% input should be string binary, like <<"appkey">>, <<"topic">>
-spec check_permission(Appkey::binary(), Topic::binary(), Token::binary(), Uid::binary(), Action::binary()) -> boolean().
check_permission(Appkey, Topic, Token, Uid, Action) ->
    if 
        undefined =:= Appkey orelse
        undefined =:= Topic orelse
        undefined =:= Action ->
           false;
       true -> eyam2_logic:check_permission(Appkey, Topic, Token, Uid, Action)
    end.

check_appkey_trigger(Appkey) ->
    eyam2_logic:get_yam_trigger_from_redis(Appkey).
