-module(eyam2_handler).

-export([init/3]).
-export([terminate/3]).
-export([allowed_methods/2]).
-export([content_types_provided/2]).
-export([content_types_accepted/2]).
-export([handle_post_request/2]).
-export([handle_get_request/2]).

-compile([{parse_transform, lager_transform}]).

-include("eyam2.hrl").

init(_type, Req, Opts) ->
    lager:debug("now init the request", []),
    Req2 = cowboy_req:set_resp_body(generate_response_body(internal_error), Req),
    {upgrade, protocol, cowboy_rest, Req2, Opts}.

allowed_methods(Req, State) ->
    {[<<"GET">>, <<"HEAD">>, <<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
    {[
        {<<"application/json">>, handle_post_request}
    ], Req, State}.

content_types_provided(Req, State) ->
    {[
        {<<"application/json">>, handle_get_request}
    ], Req, State}.

terminate(_Reason, _Req, _State) ->
    ok.

%% ============================================
%% cowboy and OTP callbacks are over
%% now we have our own logic
%% ============================================

%% Post request goes here
handle_post_request(Req,State) ->
    handle_request(Req, State).

%% Get and Head request goes here
handle_get_request(Req, State) ->
    handle_request(Req, State).

%% wrap the respond into a func
respond(Status, Body, Req) ->
    {ok, Req2} = cowboy_req:reply(Status, [{<<"content-type">>, <<"application/json">>}], Body, Req),
    Req2.

handle_request(Req,State) ->
    {ParmRet, Params, Req1} = get_parameters(Req),
    case ParmRet of
        false ->
            lager:debug("wrong param ret", []),
            Req2 = respond(400, generate_response_body(invalid_parameter) , Req1),
            {halt ,Req2, State};
        true ->
            case eyam2_logic:check_secret_key(Params#base.appkey, Params#base.seckey) of
                true ->
                    check_yam_trigger_and_pass_to_handle_command(Req, Params, State);
                _ ->
                    lager:debug("wrong sec key", []),
                    Req2 = respond(400, generate_response_body(invalid_parameter), Req1),
                    {halt,Req2, State}
            end
    end.

check_yam_trigger_and_pass_to_handle_command(Req, Params, State) ->
    if Params#base.cmd == ?CMD_GRANT orelse Params#base.cmd == ?CMD_AUDIT ->
            case eyam2_logic:get_yam_trigger_from_redis(Params#base.appkey) of
                {ok,1} ->
                    lager:debug("pass permit test"),
                    handle_command(Req, Params);
                _Other ->
                    lager:debug("unexist appkey"),
                    Req2 = respond(200, generate_response_body(not_permit), Req),
                    {halt,Req2, State}
            end;
        true ->
            handle_command(Req, Params)
    end.


handle_command(Req, Parameters) ->
    Cmd = Parameters#base.cmd,
    case Cmd of
        ?CMD_GRANT -> handle_grant(Req, Parameters);
        ?CMD_AUDIT -> handle_audit(Req, Parameters);
        ?CMD_SET -> handle_set_trigger(Req, Parameters);
        ?CMD_GET -> handle_get_trigger(Req, Parameters);
        _ -> 
            %% invalidate cmd
            lager:debug("unexpected method",[]),
            Ret = respond(400, generate_response_body(invalid_parameter) , Req),
            {halt, Ret, undefined}
    end.

%% get trigger
handle_get_trigger(Req, Parameters) ->
    Appkey = Parameters#base.appkey,
    Ret = case eyam2_etcd:get(?YAM_ETCD_PREFIX ++ "EnableAppkeyList/" ++ binary_to_list(Appkey)) of
        {ok, Value} ->
            respond(200, generate_response_body(get, Value), Req);
        _ ->
            respond(200, generate_response_body(get, 0), Req)
    end,
    {halt,Ret, undefined}.

%% set trigger
handle_set_trigger(Req, Parameters) ->
    Trigger = Parameters#base.opt,
    Appkey = Parameters#base.appkey,
    Ret = case eyam2_util:make_sure_binary(Trigger) of
        <<"0">> ->
            case eyam2_etcd:delete(?YAM_ETCD_PREFIX ++ "EnableAppkeyList/" ++ binary_to_list(Appkey)) of
                {ok, _Value} ->
                    respond(200, generate_response_body(success), Req);
                _Error ->
                    lager:debug("Return is : ~p", [_Error]),
                    respond(500, generate_response_body(internal_error), Req)
            end;
        <<"1">> ->
            case eyam2_etcd:set(?YAM_ETCD_PREFIX ++ "EnableAppkeyList/" ++ binary_to_list(Appkey), "1") of
                {ok, _Value} ->
                    respond(200, generate_response_body(success), Req);
                _Error ->
                    lager:debug("Return is : ~p", [_Error]),
                    respond(500, generate_response_body(internal_error), Req)
            end;
        _ ->
            respond(500, generate_response_body(invalid_parameter), Req)
    end,
    {halt,Ret, undefined}.

%% grant access
handle_grant(Req, Parameters) ->
    Opt = Parameters#base.opt,
    {ok, Token1} = case Opt#grant.alias of
        undefined ->
            ParamList = [{Parameters#base.appkey, Opt#grant.topic, Opt#grant.token, Opt#grant.uid, Opt#grant.w, Opt#grant.r, Opt#grant.ttl}],

            ParamList1 = 
            case Opt#grant.p of
                undefined -> ParamList;
                _ -> 
                    Topic = Opt#grant.topic,
                    [{Parameters#base.appkey, << Topic/binary, <<"/p">>/binary >>, Opt#grant.token, Opt#grant.uid, <<"0">>, Opt#grant.p, Opt#grant.ttl}| ParamList]
            end,

            handle_grant_helper(ParamList1);
        _ ->
            %% something is wrong here, token may be generated.
            Alias = Opt#grant.alias,
            ParamList = 
                    [
                     %% make it able to set the alias
                     {Parameters#base.appkey, << <<",yali">>/binary, Alias/binary >>, Opt#grant.token, Opt#grant.uid, Opt#grant.r , <<"0">>, Opt#grant.ttl},
                     {Parameters#base.appkey, << <<",yta/">>/binary, Alias/binary >>, Opt#grant.token, Opt#grant.uid, Opt#grant.w, <<"0">>, Opt#grant.ttl}
                    ],

            handle_grant_helper(ParamList)
    end,
    Ret = 
    case Token1 of
        illegal->
            lager:debug("illegal",[]),
            respond(400, generate_response_body(invalid_parameter), Req);
        undefined ->
            lager:debug("undefined",[]),
            respond(200, generate_response_body(success), Req);
        _ -> 
            respond(200, generate_response_body(grant, Token1), Req)
    end,

    {halt,Ret, undefined}.

handle_grant_helper([{Appkey, Topic, Token, Uid, W, R, TTL}|Tail]) ->
    case handle_grant_do(Appkey, Topic, Token, Uid, W, R, TTL) of
        {ok, illegal} ->
            {ok, illegal};
        {ok, Token1} ->
            lager:debug("get token ~p",[Token1]),
            handle_grant_helper(Token1, Tail)
    end;
handle_grant_helper([]) ->
    {ok, illegal}.

%% Token can be undefined
handle_grant_helper(Token, [{Appkey, Topic, _, Uid, W, R, TTL}|Tail]) ->
    case handle_grant_do(Appkey, Topic, Token, Uid, W, R, TTL) of
        {ok, illegal} ->
            {ok, illegal};
        {ok, Token1} ->
            handle_grant_helper(Token1, Tail)
    end;

handle_grant_helper(Token, []) ->
    {ok, Token}.

handle_grant_do(Appkey, Topic, Token, Uid, W, R, TTL) ->
    Token1 = case Token of
                 undefined -> undefined;
                 <<"">> -> 
                     lager:debug("new token"),
                     NewToken = eyam2_logic:create_new_token(Appkey, Topic, TTL),
                     TokenKey = eyam2_logic:get_token_key(NewToken),
                     eyam2_util:redis_set(TokenKey, 1, TTL),
                     NewToken;
                 _ -> case eyam2_logic:token_exists(Token) of
                          true -> 
                              lager:debug("old token"),
                              %% refresh the ttl of the token
                              TokenKey = eyam2_logic:get_token_key(Token),
                              eyam2_util:redis_set(TokenKey, 1, TTL),
                              Token;
                          false -> 
                              lager:debug("illega token ~p", [Token]),
                              illegal
                      end
             end,
    case Token1 of
        illegal -> {ok, illegal};
        _ ->
            lager:debug("token is ~p", [Token1]),
            GrantKey = eyam2_logic:get_grant_key(Appkey, Topic, Token1, Uid),
            Trigger = eyam2_logic:rw_to_integer(R, W),
            eyam2_util:redis_set(GrantKey, Trigger, eyam2_util:make_sure_integer(TTL)),
            {ok, Token1}
        end.

handle_audit(Req, Parameters) ->
    Opt = Parameters#base.opt,
    Ret = 
    case Opt#grant.alias of
        undefined ->
            CheckList = [{Parameters#base.appkey, Opt#grant.topic, Opt#grant.token, Opt#grant.uid}],
            CheckList1 = 
                case Opt#grant.topic of
                    undefined -> CheckList;
                    _ -> 
                        Topic = Opt#grant.topic,
                        CheckList ++ [{Parameters#base.appkey, << Topic/binary, <<"/p">>/binary>>, Opt#grant.token, Opt#grant.uid}]
                end,
            case handle_audit_helper(CheckList1) of
                {not_permit} ->
                    respond(200, generate_response_body(not_permit), Req);
                [{ReadRet, WriteRet}] ->
                    % topic level audit, so presence is equal to read ret
                    respond(200, generate_response_body(audit, ReadRet, WriteRet, ReadRet), Req);
                [{ReadRet, WriteRet}, {PresenceRet, _}] ->
                    respond(200, generate_response_body(audit, ReadRet, WriteRet, PresenceRet), Req)
            end;
        _ ->
            Alias = Opt#grant.alias,
            CheckList = [
                         {Parameters#base.appkey, << <<",yali">>/binary, Alias/binary >>, Opt#grant.token, Opt#grant.uid},
                         {Parameters#base.appkey, << <<",yta/">>/binary, Alias/binary>>, Opt#grant.token, Opt#grant.uid}
                        ],
            case handle_audit_helper(CheckList) of
                {not_permit} ->
                    respond(200, generate_response_body(not_permit), Req);
                [{_, ReadRet}, {_, WriteRet}] ->
                    respond(200, generate_response_body(audit, ReadRet, WriteRet ), Req)
            end
    end,
    {halt,Ret, undefined}.

handle_audit_helper([]) ->
    [];
handle_audit_helper([{Appkey, Topic, Token, Uid}|Tail]) ->
    case eyam2_logic:check_permission(Appkey, Topic, Token, Uid) of
        {ok,{ReadRet, WriteRet}} ->
            lager:debug(" ~p : R, W is: ~p", [{Appkey, Topic, Token}, {ReadRet, WriteRet}]),
            [{ReadRet, WriteRet}|handle_audit_helper(Tail)];
        {not_permit} -> {not_permit}
    end.


%% these logic are messy
%% get parameters from body or query values
%% return {bool, parameter, Req}
get_parameters(Req) ->
    {Result, Params, ReqNext} = case cowboy_req:has_body(Req) of
        true ->
            case cowboy_req:body(Req) of
                {ok, Body, Req1} ->
                    case get_parm_from_json(Body) of
                        {true, P} -> {true, P, Req1};
                        _ -> {false,<<"invalidate json">>,Req1}
                    end;
                {error, Error} ->
                    {false, atom_to_binary(Error, latin1), Req}
            end;
        false ->
            %% no body, so we try to get it from qs
            {GetVals, Req1} = cowboy_req:qs_vals(Req),
            {true, get_yam_params(GetVals), Req1}
    end,
    case Result of
        true -> 
            case validate_params(Params) of
                {true} -> 
                    {true, Params, ReqNext};
                {false, _}-> {false, undefined, ReqNext}
            end;
        false -> 
            {false, undefined, ReqNext}
    end.

%% get parameters from json only
get_parm_from_json(Body) ->
    case jiffy:decode(Body) of
        {Props} ->
            {true, get_yam_params(Props)};
        _ ->
            {false, <<"Wrong json format">>}
    end.

get_yam_params(Props) ->
    Appkey = proplists:get_value(<<"appkey">>, Props),
    Cmd = proplists:get_value(<<"method">>, Props),
    Seckey = proplists:get_value(<<"seckey">>, Props),
    Trigger = proplists:get_value(<<"trigger">>, Props),
    W = proplists:get_value(<<"w">>, Props),
    R = proplists:get_value(<<"r">>, Props),
    P = proplists:get_value(<<"p">>, Props),
    Uid = proplists:get_value(<<"uid">>, Props),
    TTL = case proplists:get_value(<<"ttl">>, Props) of
        undefined -> undefined;
        ExistTTL -> 
            BinaryTTL = eyam2_util:make_sure_binary(ExistTTL),
            case BinaryTTL of
                <<"0">> -> 
                    %% set to 2 days if the input is zero, we don't allow unlimited ttl
                    %% 2 * 24 * 60 * 60 = 172800
                    <<"172800">>;
                OtherTTL -> OtherTTL
            end
    end,
    Topic = proplists:get_value(<<"topic">>, Props),
    Token = proplists:get_value(<<"token">>, Props),
    Alias = proplists:get_value(<<"alias">>, Props),

    Opt = case Cmd of
        ?CMD_SET ->
            Trigger;
        ?CMD_GET ->
            undefined;
        _ ->
            #grant{ ttl = TTL, topic = Topic, token = Token, alias = Alias, uid = Uid, w = W, r = R, p = P}
    end,
    lager:debug("props is: ~p", [Props]),
    #base{appkey = Appkey, cmd = Cmd, seckey = Seckey, opt = Opt}.

validate_params(Params) ->
    Opt = Params#base.opt,
    if
        %These commands should be admin command, so any of it should be presented in request
        Params#base.cmd == undefined orelse Params#base.appkey == undefined orelse Params#base.seckey == undefined->
            lager:debug("something important undefined ~p",[Params]),
            {false, <<"invalied parameters, missing cmd , appkey or secretkey">>};
        Params#base.cmd == ?CMD_GET ->
            {true};
        Params#base.cmd == ?CMD_SET ->
            case Opt of
                undefined ->
                    lager:debug("something important undefined ~p",[Params]),
                    {false, <<"invalied parameters, missing cmd , appkey or secretkey">>};
                _ ->
                    {true}
            end;
        true ->
            if Opt#grant.uid =/= undefined andalso Opt#grant.token =/= undefined ->
                   lager:debug("Shoud not use uid and token together ~p",[Params]),
                   {false, <<"invalied parameters, conflict params">>};
               Opt#grant.alias =/= undefined andalso Opt#grant.topic =/= undefined ->
                   lager:debug("Shoud not use topic and alias together ~p",[Params]),
                   {false, <<"invalied parameters, conflict params">>};
               Opt#grant.token =/= undefined andalso Opt#grant.topic == undefined andalso Opt#grant.alias == undefined->
                   lager:debug("token without topic",[]),
                   {false, <<"invalied parameters">>};
               true ->
                   case Params#base.cmd of
                       ?CMD_GRANT when Opt#grant.w == undefined orelse Opt#grant.r == undefined orelse Opt#grant.ttl == undefined->
                           lager:debug("something undefined",[]),
                           {false, <<"invalied parameters, missing w,r or ttl">>};
                       ?CMD_GRANT when Opt#grant.token =/= undefined andalso Opt#grant.topic == undefined andalso Opt#grant.alias == undefined->
                           lager:debug("token without topic",[]),
                           {false, <<"invalied parameters">>};
                       ?CMD_GRANT when Opt#grant.token == undefined andalso Opt#grant.topic == undefined andalso Opt#grant.alias == undefined andalso Opt#grant.p =/= undefined->
                           lager:debug("presence is bind with read when you are granting to appkey level",[]),
                           {false, <<"invalied parameters">>};
                       _ ->
                           {true}
                   end
            end
    end.

generate_response_body(invalid_parameter) ->
    jiffy:encode({[{<<"status">>,1}, {<<"error">>,<<"invalid parameters">>}]});
generate_response_body(internal_error) ->
    jiffy:encode({[{<<"status">>,2}, {<<"error">>,<<"internal error">>}]});
generate_response_body(not_permit) ->
    jiffy:encode({[{<<"status">>,3}, {<<"error">>,<<"not permitted">>}]});
generate_response_body(success) ->
    jiffy:encode({[{<<"status">>,0}]}).


generate_response_body(audit, R, W,P) ->
    jiffy:encode({[{<<"status">>,0}, {<<"w">>, eyam2_util:make_sure_integer(W)}, {<<"r">>, eyam2_util:make_sure_integer(R)}, {<<"p">>, eyam2_util:make_sure_integer(P)}]}).


generate_response_body(audit, R, W) ->
    jiffy:encode({[{<<"status">>,0}, {<<"w">>, eyam2_util:make_sure_integer(W)}, {<<"r">>, eyam2_util:make_sure_integer(R)}]}).

generate_response_body(get, T) ->
    jiffy:encode({[{<<"status">>,0}, {<<"trigger">>, eyam2_util:make_sure_integer(T)}]});
generate_response_body(grant, Token) ->
    jiffy:encode({[{<<"status">>,0}, {<<"token">>, Token}]}).
