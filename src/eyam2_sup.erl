-module(eyam2_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).
-define(CHILD(I, Type, Args), {I, {I, start_link, Args}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, { {one_for_one, 5, 10}, [
        ?CHILD(mongodberl, worker, get_mongo_start_args()),
        ?CHILD(eyam2_etcd, worker, [])
    ]} }.

get_mongo_start_args() ->
    {UriList, DBName, ConnectionNum } =
        application:get_env(eyam2, mongodberl_yam, {
            ["localhost:27017"],
            "drywall" ,
            5
        }),
    MongodbArg = {repl,UriList},
    [{replset, {mongodbpool,MongodbArg, DBName, ConnectionNum}}].
