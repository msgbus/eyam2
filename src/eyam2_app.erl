-module(eyam2_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

-compile([{parse_transform, lager_transform}]).

%% ===================================================================
%% Application callbacks
%% ===================================================================
start(_StartType, _StartArgs) ->
    {ok, _} = start_eredis(),
    {ok, _} = start_cowboy(),
    eyam2_sup:start_link().

stop(_State) ->
    ok.

%% ==================================================================
%% custom inits
%% ==================================================================
start_cowboy() ->
    Dispatch = cowboy_router:compile([
        {'_', [
            {"/", eyam2_handler, []}
        ]}
    ]),
    HttpPort = application:get_env(eyam2, port, 8099),
    cowboy:start_http(http, 100, [{port, HttpPort}], [
        {env, [{dispatch, Dispatch}]}
    ]).

start_eredis() ->
    {Host, Port, Size, MaxOverflow} =
            application:get_env(eyam2, redis_yam, {"localhost", 6379, 5, 10}),
    Ret = eredis_pool:create_pool(eyam,{Size, MaxOverflow}, Host, Port),
    Ret.





