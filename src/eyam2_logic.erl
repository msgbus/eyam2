-module(eyam2_logic).

-compile([{parse_transform, lager_transform}]).

-export([check_secret_key/2, get_yam_trigger_from_redis/1, get_token_key/1,check_permission/4, check_permission/5, rw_to_integer/2, get_grant_key/4, create_new_token/3, token_exists/1]).

%export for testing
-export([get_yam_state_from_etcd/1]).
-include("eyam2.hrl").

%% This func is not using cache
check_secret_key(Appkey, SecKey) ->
    %% mock to true for testing perpose
    TestAppkey = application:get_env(eyam2, test_appkey, undefined),
    if TestAppkey =/= undefined andalso Appkey == TestAppkey -> true;
        true -> 
            case eyam2_util:redis_get(Appkey) of
                {ok, Value} when Value =/= undefined ->
                    Value =:= SecKey;
                _ ->
                    case mongodberl:get_value_from_mongo(mongodbpool, seckey, Appkey) of
                        {true, Value} ->
                            ToReturn = 
                            if Value =:= SecKey ->
                                    true;
                                true ->
                                    false
                            end,

                            %% set cache
                            eyam2_util:redis_set(Appkey, Value, <<"0">>),
                            ToReturn;
                        _Else ->
                            lager:error("get secert key from mongodb error"),
                            error
                    end
            end
    end.

-spec get_yam_state_from_etcd(binary()) -> {fail} | {ok, binary()}.
get_yam_state_from_etcd(Key)->
    KeyInListType = binary_to_list(Key),
    case eyam2_etcd:get(?YAM_ETCD_PREFIX ++ "EnableAppkeyList/" ++ KeyInListType) of
        {ok, <<"0">>} -> {ok, false};
        {ok, <<"1">>} -> {ok, true};
        {fail,not_found} -> {ok, false};
        _Err -> 
            lager:debug("return error:~p", [_Err]),
            {fail}
    end.


%% will also set the trigger if the key is not hit
get_yam_trigger_from_redis(Key) ->
    %% this is mock to 1 so I won't need to check the appkey in mongodb any more
    TestAppkey = application:get_env(eyam2, test_appkey, undefined),
    lager:debug("testAppkey :~p, key ~p", [TestAppkey, Key]),
    if TestAppkey =/= undefined andalso Key == TestAppkey -> {ok, 1};
       true -> 
           lager:debug("checking key: ~p", [Key]),
           RedisKey = << <<"YAMT_">>/binary, Key/binary>>,

           QueryFun = 
           fun() ->
                   case ?MODULE:get_yam_state_from_etcd(Key) of
                       {ok, true} -> {ok, 1};
                       {ok, false} -> {ok, 0};
                       _ -> {ok, 0}
                   end
           end,
           case eyam2_util:redis_get_and_query(RedisKey, <<"300">>, QueryFun) of
               {ok, Value} -> 
                   Ret = eyam2_util:make_sure_integer(Value),
                   {ok, Ret};
               Other -> Other
           end
    end.

get_grant_key(Appkey, Topic, Token, Uid) ->
    case {Appkey, Topic, Token, Uid} of
        {_, undefined, undefined, undefined} ->
            %% grant on appkey level
            << <<"A_">>/binary, Appkey/binary>> ;
        {_, _, undefined, undefined} ->
            << <<"C_">>/binary , Appkey/binary , <<"_">>/binary , Topic/binary>>;
        {_, _, _, undefined} ->
            << <<"U_">>/binary, Appkey/binary, <<"_">>/binary , Topic/binary, <<"_">>/binary, Token/binary>>;
        {_, _, undefined, _} ->
            << <<"I_">>/binary, Appkey/binary, <<"_">>/binary , Topic/binary, <<"_">>/binary, Uid/binary>>;
        _ ->
            lager:error("get error, unknown input: ~p,~p,~p,~p", [Appkey, Topic, Token, Uid]),
            error
    end.

get_token_key(Token) ->
    << <<"K_">>/binary, Token/binary>>.

rw_to_integer(R, W) ->
    ReadTrigger = eyam2_util:make_sure_integer(R) bsl 1,
    WriteTrigger = eyam2_util:make_sure_integer(W),
    ReadTrigger + WriteTrigger.

%% return {ok, ReadPermit, WritePermit} or {not_permit}
check_permission(Appkey, Topic, Token, Uid) ->
    Permit = check_permission_internal(Appkey, Topic, Token, Uid),
    {ok, Permit}.

%% return true or false
check_permission(Appkey, Topic, Token, Uid, Action) ->
    if Topic == <<",yali">> ->
           %% always return true for unset topic
           true;
       true ->
           permission_to_bool(check_permission_internal(Appkey, Topic, undefined, undefined), Action) orelse
           permission_to_bool(check_permission_internal(Appkey, Topic, Token, undefined), Action) orelse
           permission_to_bool(check_permission_internal(Appkey, Topic, undefined, Uid), Action) orelse
           permission_to_bool(check_permission_internal(Appkey, undefined, undefined, undefined), Action)
    end.

check_permission_internal(Appkey, Topic, Token, Uid) ->
    TokenCheckPass = 
    case Token of
        undefined -> true;
        _ -> token_exists(Token)
    end,
    case TokenCheckPass of
        true ->
           GrantKey = get_grant_key(Appkey, Topic, Token, Uid),
           Permission = case eyam2_util:redis_get(GrantKey) of
                            {ok, P} when P =/= undefined-> 
                                eyam2_util:make_sure_integer(P);
                            _ -> 0
                        end,
           ReadPer = Permission bsr 1, 
           WritePer = Permission band 2#01,
           {ReadPer, WritePer};
       false ->
           {0, 0}
    end.

permission_to_bool(Permit, Action) ->
    {R,W} = Permit,
    case Action of
        <<"r">> -> R =:= 1;
        <<"w">> -> W =:= 1;
        _ -> false
    end.

create_new_token(Appkey, Topic, TTL) ->
    %% use appkey, topic, ttl, millionseconds, and a random number to a hash code to avoid confilct
    {_,_,Ms} = os:timestamp(),
    BinaryMs = integer_to_binary(Ms),
    RandBinary = integer_to_binary(crypto:rand_uniform(1, 10000)),
    TokenBinary = crypto:hash(md5, <<Appkey/binary, Topic/binary, TTL/binary, BinaryMs/binary, RandBinary/binary>> ),
    Token = [begin if N < 10 -> 48 + N; true -> 87 + N end end || <<N:4>> <= TokenBinary],
    lager:debug("token is ~s", [Token]),
    list_to_binary(Token).

token_exists(Token) ->
    case eyam2_util:redis_get(<< <<"K_">>/binary, Token/binary>>) of
        {ok, undefined} -> false;
        {ok, _Value} ->
            true;
        _Ret -> 
            lager:debug("unknow ret: ~p", [_Ret]),
            false
    end.
