-module(eyam2_util).

-compile([{parse_transform, lager_transform}]).

-export([redis_auth/1, redis_get_and_query/3, make_sure_binary/1, make_sure_integer/1, redis_set/3, redis_get/1, redis_del/1 ]).

redis_auth(Pwd) ->
    Ret = eredis_pool:q(eyam, ["AUTH", Pwd]),
    lager:debug("Ret is : ~p", Ret),
    Ret.

%% save to redis
redis_set(Key, Value, TTL) ->
    BinTTL = make_sure_binary(TTL),
    case BinTTL of
        <<"0">> -> 
            lager:debug("ttl is zero:~p", [TTL]),
            eredis_pool:q(eyam, ["SET", Key, Value]);
        undefined ->
            lager:debug("ttl is zero:~p", [TTL]),
            eredis_pool:q(eyam, ["SET", Key, Value]);
        _ -> 
            lager:debug("ttl is not zero:~p", [TTL]),
            eredis_pool:q(eyam, ["SET", Key, Value]),
            eredis_pool:q(eyam, ["EXPIRE", Key, TTL])
    end.

redis_get(Key) ->
    eredis_pool:q(eyam, ["GET", Key]).

redis_get_and_query(Key, TTL, Fun) ->
    case redis_get(Key) of
        {ok, Value} when Value =/= undefined ->
            lager:debug("cache hit, get redis value: ~p", [Value]),
            {ok, Value};
        _ ->
            lager:debug("no cache hit, go into fun"),
            {ok, Ret} = Fun(),
            lager:debug("the result is :~p", [Ret]),
            redis_set(Key, make_sure_binary(Ret), make_sure_binary(TTL)),
            {ok,Ret}
    end.

redis_del(Key) ->
    eredis_pool:q(eyam, ["DEL", Key]).

make_sure_integer(I) ->
    if is_binary(I) -> binary_to_integer(I);
       true -> I
    end.

make_sure_binary(I) ->
    if is_integer(I) -> integer_to_binary(I);
       is_float(I) -> integer_to_binary(round(I));
       is_list(I) -> list_to_binary(I);
       is_tuple(I) -> list_to_binary(tuple_to_list(I));
       is_binary(I) -> I;
       true -> I
    end.
