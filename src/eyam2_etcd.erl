% this etcd util is written for eyam2, I should rewrite a new etcd for
-module(eyam2_etcd).

-compile([{parse_transform, lager_transform}]).

-behavior(gen_server).

%% gen_server callbacks
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

-define(SERVER, ?MODULE).

%%%===================================================================
%%% API
%%%===================================================================
-export([start_link/0, set/3, set/2, get/1, delete/1, watch/2]).

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

-spec set(Key::list(), Value::list(), TTL::integer()) -> ok.
set(Key, Value, TTL) ->
    gen_server:call(?MODULE, {set, Key, Value, TTL}).

-spec set(Key::list(), Value::list()) -> ok.
set(Key, Value ) ->
    gen_server:call(?MODULE, {set, Key, Value}).

-spec get(Key::list()) -> {ok, list()}.
get(Key) ->
    gen_server:call(?MODULE, {get, Key}).

-spec delete(Key::list()) -> ok.
delete(Key) ->
    gen_server:call(?MODULE, {delete, Key}).

-spec watch(Key::list(), Callback::fun((binary())->ok)) -> ok.
watch(Key, Callback) ->
    gen_server:cast(?MODULE, {watch, Key, Callback}).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

init([]) ->
    Etcds = application:get_env(eyam2, etcd_addr, ["http://localhost:2379"]), 
    Peer = lists:foldl(
        fun(Url, ActivePeer) -> 
            case ActivePeer of
                undefined ->
                    case check_peer_alive(Url) of
                        true -> Url;
                        false -> undefined
                    end;
                _ ->
                    ActivePeer
            end
        end, undefined, Etcds),
    {ok, [{appkey_set, sets:new()}, {peer, Peer}, {etcds, Etcds}]}.

check_peer_alive(Url) ->
    case ibrowse:send_req(Url ++ "/version", [], get, [], [], 5000) of
        {ok, ReturnCode, _Headers, _Body} ->
            case ReturnCode of
                "200" -> true;
                _ -> false
            end;
        _ ->
            false
    end.


handle_call(Request, _From, State) ->
    UrlForV2 = proplists:get_value(peer,State) ++ "/v2", 
    Reply = case Request of
        {set, Key, Value, TTL} ->
            etcd_action(set, UrlForV2, Key, Value, integer_to_list(TTL));
        {set, Key, Value} ->
            etcd_action(set, UrlForV2, Key, Value, "");
        {get, Key} ->
            etcd_action(get, UrlForV2, Key);
        {delete, Key} ->
            etcd_action(delete, UrlForV2, Key)
    end,

    {reply, Reply, State}.

handle_cast(Msg, State) ->
    case Msg of
        {watch, Key, Callback} ->
            ok;
        _ ->
            ok
    end,
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
etcd_action(set, Url, Key, Value, TTL) ->
    Header = [{"Content-Type", "application/x-www-form-urlencoded"}],
    TTLStr = case TTL of
        "" -> "";
       _ -> "&ttl=" ++ TTL
    end,
    Body = "value=" ++ Value ++ TTLStr, 
    case ibrowse:send_req(Url ++ "/keys" ++ Key, Header, put, Body, [], 5000) of
        {ok, ReturnCode, _Headers, RetBody} ->
            case ReturnCode of
                "200" -> 
                    {ok, RetBody};
                "201" -> 
                    {ok, RetBody};
                _ -> 
                    lager:debug("return code: ~p", ReturnCode),
                    {fail, wrong_response_code}
            end;
        _Error ->
            lager:debug("set error ~p",[_Error] ),
            {fail, no_response}
    end.

%% {"action":"get","node":{"key":"/message","value":"Hello world","modifiedIndex":4,"createdIndex":4}}
%% {"errorCode":100,"message":"Key not found","cause":"/message","index":312611}
etcd_action(get, Url, Key) ->
    case ibrowse:send_req(Url ++ "/keys" ++ Key, [], get, [], [], 5000) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" -> 
                    BinBody = list_to_binary(Body),
                    case jiffy:decode(Body) of
                        {Props} ->
                            %% no error, return value
                            {NodeValue} = proplists:get_value(<<"node">>, Props),
                            Value = proplists:get_value(<<"value">>, NodeValue),
                            {ok, Value};
                        _ ->
                            {fail, wrong_json_body}
                    end;
                "404" ->
                    {fail, not_found};
                _ -> 
                    lager:debug("response code :~p", [ReturnCode]),
                    {fail, wrong_response_code}
            end;
        _ ->
            {fail, no_response}
    end;
etcd_action(delete,Url, Key) ->
    lager:debug("url and key is:~p~p", [Url, Key]),
    case ibrowse:send_req(Url ++ "/keys" ++ Key, [], delete, [], [], 5000) of
        {ok, ReturnCode, _Headers, Body} ->
            case ReturnCode of
                "200" -> 
                    {ok, Body};
                "404" -> 
                    {ok, Body};
                _ -> 
                    lager:debug("response code :~p", [ReturnCode]),
                    {fail, wrong_response_code}
            end;
        _ ->
            {fail, no_response}
    end.
