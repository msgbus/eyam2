### rpc for YAM

```
eyam2_remote:check_permission(
	Appkey::binary(),
	Topic::binary(), 
	Token::binary(), 
	Uid::binary(), 
	Action::binary()) 
	
	-> boolean().
```

其中Action只允许 `<<"w">>` 和 `<<"r">>` 两个输入。

