## Restful API Examples

直接看例子。具体字段含义参看本文最后的字段含义表。

## mqtt内携带token的方法
对于sub, unsub, publish三个命令，将原有的topic（命名为orig_topic）变成以下格式：

`,yam${token}_${orig_topic}`

就可以携带token了。例如下面这样。

`,yamOKHIKNNH1250skadfKDJFE_test_topic`

上述topic会携带token `OKHIKNNH1250skadfKDJFE` ，读/写 `test_topic` 这个topic

**如果yam内设置了topic级别或者app级别的权限控制，则不需要携带token，使用原有的topic即可**

## rest请求格式：

测试地址：

`http://yam-test.yunba.io`

请求头：

`Content-Type:Application/json`

Body请按照下面的例子填写合适的json。

## 操纵APP级别的读写:

```json
{
    "appkey": "58072d1fd69873332db470a6",
    "seckey": "sec-ICanNotTellYou",
    "method":"yam_grant",
    "r":1,
    "w":1,
    "ttl":100
}
```

上述请求将会将appkey为`58072d1fd69873332db470a6` 的APP的所有topic都设置为可读可写，持续100秒。相当于暂时废除了权限管理。

## 操纵Topic级别的读写：

```json
{
    "appkey": "58072d1fd69873332db470a6",
    "seckey": "sec-ICanNotTellYou",
    "method":"yam_grant",
    "topic":"test_topic",
    "r":1,
    "w":0,
    "ttl":100
}
```

上述请求会将appkey为`58072d1fd69873332db470a6` 的APP下面的`test_topic`这个topic设置为所有人可读。但是并不是所有人可写。

## 操纵Alias级别的读写：

```json
{
    "appkey": "58072d1fd69873332db470a6",
    "seckey": "sec-ICanNotTellYou",
    "method":"yam_grant",
    "alias":"test_alias",
    "r":1,
    "w":0,
    "ttl":100
}
```

上述请求会将appkey为`58072d1fd69873332db470a6` 的APP下面的`test_alias`这个topic设置为所有人可读。但是并不是所有人可写。即，所有人都可以将自己设置为`test_alias`，但是没有人可以给这个alias上发消息。

**alias和topic是同一个层级的概念，两者不可以同时出现**

## 申请一个token

```json
{
    "appkey": "58072d1fd69873332db470a6",
    "seckey": "sec-ICanNotTellYou",
    "method":"yam_grant",
    "topic":"test_topic",
    "token":"",
    "r":1,
    "w":0,
    "ttl":100
}
```

上述请求会将返回一个token，该token可以在appkey为 `58072d1fd69873332db470a6` 的APP下面的`test_topic` 这个topic下面进行读操作。成功时返回如下：

```json
{
	"status":0,
	"token":"342251e5c3f547f24c91e9a97356ae1f"
}
```

## 为一个token附加更多的权限：

```json
{
    "appkey": "58072d1fd69873332db470a6",
    "seckey": "sec-ICanNotTellYou",
    "method":"yam_grant",
    "topic":"another_topic",
    "token":"342251e5c3f547f24c91e9a97356ae1f",
    "r":1,
    "w":0,
    "ttl":100
}
```

这个请求会为 `342251e5c3f547f24c91e9a97356ae1f` 这个token附加 `another_topic`这个topic的读权限。

**申请token和为token附加权限的请求差别在于申请的时候token字段出现，但是内容为空字符串**

**token的有效期按照最后一次请求的ttl计算**

## 检查某个token的权限情况：

```json
{
    "appkey": "58072d1fd69873332db470a6",
    "seckey": "sec-ICanNotTellYou",
    "method":"yam_audit",
    "topic":"another_topic",
    "token":"342251e5c3f547f24c91e9a97356ae1f",
}
```

返回内容：

```json
{
	"status": 0,
	"r" :1,
	"w" :0,
	"p" :0
}
```

总的来说，检查权限的接口就是将授权的接口的`method`字段从`yam_grant`修改为`yam_audit`，并且去除`w`,`r`,`ttl`等字段即可。返回内容会告知某个层级的具体读写权限。

**检查app级别，topic级别的权限，做类似修改即可。**

## rest字段含义表：

字段名 | 字段含义
------- | -------
appkey | 具体的appkey
seckey | appkey对应的secret key
method | 请求的方法
topic| 控制的主题
alias | 别名
p | 读presence权限
r | 读权限
w | 写权限
ttl | time to live，权限的有效期限。单位为秒

