# elogic集成eyam2

## 集成思路

目前集成的方法是使用topic来带token。对于有token的client，只要在publish或者sub的时候按照下列规则定义topic即可：

`,yam{Token}_{OrgTopic}`

使用 `,yam` 作为前缀，然后紧跟着token，之后紧跟一个下划线。下划线之后为原本的topic，也就是OrgTopic。

如果没有token可以不带token，直接发送原本的topic就认为没有token直接发送。

特别地，对于 alais set 操做，使用`,yam_{Token}_{OrgTopic}{Payload}` 的topic，原本逻辑不变，会在elogic内部进行处理。

## 代码逻辑

目前集成publish的逻辑在logic_publish的handle_publish_for_topic里面，先判断topic然后再决定是不是走下面的逻辑。但是这样导致他不会有ack或者rep。subscribe集成在logic_subscribe_unsub的do_sub_unsub里面。但是实际上do_sub_unsub有一个问题，就是一个topic失败，整个suback就不发了，导致无权限直接失败。

具体这个要怎么fit到mqtt的协议里面需要考虑一下。

## 存在问题

1，publish和subscribe的ack包有时候不能下发。
2，token在topic里面导致publish和subscribe包变大。
3，每次请求都必须校验token。
4，目前的rpc请求节点信息是在配置里的，每次只配置了一个节点，要不要考虑服务发现然后扩展呢。

