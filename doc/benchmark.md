#Benchmark for eyam2

测试环境：

```
虚拟机：vmware fusion 8
内存：2048MB
处理器：1个处理器核心
系统：64位ubuntu 12.04
erlang：16B02 hipe

host机： Macbook pro 2015
OSX版本：10.11.14
处理器： 2.7 GHz Intel Core i5
内存： 8 GB 1867MHz DDR3
```

eyam2运行于虚拟机内，从host机使用http_load发起测试。所有测试均适用100个并行

## grant

直接grant，不申请token。

`http://192.168.175.137:8099/?appkey=58072d1fd69873332db470a6&seckey=sec-YCQmkjDLLxfkSfrd90sUCmngPDlAhBODGCp3fFnLy2n5fPXH&method=yam_grant&ttl=0&w=0&r=1`

```
29390 fetches, 100 max parallel, 411460 bytes, in 60.0041 seconds
14 mean bytes/connection
489.8 fetches/sec, 6857.2 bytes/sec
msecs/connect: 0.94685 mean, 113.206 max, 0.151 min
msecs/first-response: 202.605 mean, 1720.81 max, 34.841 min
HTTP response codes:
  code 200 -- 29390
```

使用grant，每次申请新token

`http://192.168.175.137:8099/?appkey=58072d1fd69873332db470a6&seckey=sec-YCQmkjDLLxfkSfrd90sUCmngPDlAhBODGCp3fFnLy2n5fPXH&method=yam_grant&ttl=0&w=0&r=1&topic=test_topic&token=`

```
26259 fetches, 100 max parallel, 1.49676e+06 bytes, in 60.003 seconds
57 mean bytes/connection
437.628 fetches/sec, 24944.8 bytes/sec
msecs/connect: 1.09863 mean, 1127.54 max, 0.159 min
msecs/first-response: 226.841 mean, 1003.09 max, 58.343 min
HTTP response codes:
  code 200 -- 26259
```

使用grant，每次带一个已经申请好的token

`http://192.168.175.137:8099/?appkey=58072d1fd69873332db470a6&seckey=sec-YCQmkjDLLxfkSfrd90sUCmngPDlAhBODGCp3fFnLy2n5fPXH&method=yam_grant&ttl=0&w=0&r=1&topic=test_topic&token=ef14809ec35a8ba1d9bbb21dd960dffb`

```
25497 fetches, 100 max parallel, 1.45333e+06 bytes, in 60.0008 seconds
57 mean bytes/connection
424.944 fetches/sec, 24221.8 bytes/sec
msecs/connect: 1.16319 mean, 1124.74 max, 0.142 min
msecs/first-response: 233.492 mean, 1138.43 max, 78.205 min
HTTP response codes:
  code 200 -- 25497
```

## audit

直接对某个token进行audit：

`http://192.168.175.137:8099/?appkey=58072d1fd69873332db470a6&seckey=sec-YCQmkjDLLxfkSfrd90sUCmngPDlAhBODGCp3fFnLy2n5fPXH&method=yam_audit&topic=test_topic&token=ef14809ec35a8ba1d9bbb21dd960dffb
`

```
30494 fetches, 100 max parallel, 1.15877e+06 bytes, in 60.0232 seconds
38 mean bytes/connection
508.037 fetches/sec, 19305.4 bytes/sec
msecs/connect: 2.65562 mean, 1315.81 max, 0.156 min
msecs/first-response: 193.267 mean, 677.809 max, 35.189 min
HTTP response codes:
  code 200 -- 30494
```

## prc接口测试：

使用脚本为 [protoo.erl](protool.erl) ，该脚本分别进行单线程1w次的rpc:call，以及100个spawn，每个spawn 100个rpc:call，执行结果如下：

```shell
➜  doc git:(develop) ✗ ./protool.erl
one loop for 1w request 2968851 ms 
100 loop for 100 request 2743001 ms 
➜  doc git:(develop) ✗ ./protool.erl
one loop for 1w request 2874764 ms 
100 loop for 100 request 2854464 ms 
➜  doc git:(develop) ✗ ./protool.erl
one loop for 1w request 2740446 ms 
100 loop for 100 request 2696673 ms 
➜  doc git:(develop) ✗ ./protool.erl
one loop for 1w request 2907550 ms 
100 loop for 100 request 2676174 ms
```

