##RESTful API for YAM

**所有yam的rest接口都默认是admin模式，需要seckey和appkey，所以如无特殊说明，以下所有接口都默认带有seckey和appkey参数。** 

所有功能使用method参数声明其行为，所以也默认带有method参数。

所有接口同时支持get方法与post方法。测试可以使用get方法，实际使用请用post方法。

所有接口的响应，如无例外，遵循以下原则：

- 总是反回一个json。除非内部错误，返回5xx，其他情况一律返回200状态码。
- 返回的json必定包含一个status关键字
- status为0表示成功，如无特殊说明，不会有其他字段。
- status非零表示出现错误，会有一个error关键字声明具体出现了什么错误。
- w,r,p,ttl几个参数同时接受string和int类型的输入。但是返回一律返回int类型。

#### yam_set
使能或去使能yam功能。

这个接口不由yam维护，直接由portal维护。

#### yam_get
检查yam使能状态。

请求参数：

参数名 | 类型 | 说明
------- | ------- | -------
method | string | 必须使用"yam_get"

使用get请求：

```
http://localhost:8099/?appkey=581953df1a9bd33b8700e203&seckey=123456&method=yam_get
```

使用post请求时的content：

```
{
"method":"yam_get",
"appkey":"581953df1a9bd33b8700e203",
"seckey":"123456"
}
```

成功时额外返回参数：

参数名 | 类型 | 说明
------- | ------- | -------
trigger | string | "1"：使能， “0”：去使能

#### yam_grant

为`app`，`topic`或者`topic下的某个token`赋予访问权限。

参数：

参数名 | 类型 | 说明
------- | ------- | -------
w | string | "1"：允许写， “0”：禁止写
r | string | "1"：允许读， “0”：禁止读
method | string | 必须使用"yam_grant"
topic | string | 可选参数。
alias | string | 可选参数。
token | string | 可选参数
uid | string | 可选参数
p | string | 可选参数，"1"：允许读presence， ”0“：禁止读presence

对于授权方式，具体规则如下：
1，如果请求中没有出现topic和token/uid，则认为在app层次上授权。
2，如果请求中没有出现token/uid，只出现topic，则认为在topic层次上授权。
3，如果请求没出现topic，只有token/uid，则返回参数错误。
4，如果请求中出现token/uid和topic，则认为token/uid授予对应channel的权限。
5，如果token出现但是为空字符串，则认为生成新的token，否则认为给已经存在的token授权。
6, 注意，token的存活时间为左后一个grant的时候设置的ttl,之前哪怕设置了再长的ttl都会因为最后一次修改而失效
7, token和uid在一次请求中只允许出现一次

alias和topic同时只能出现一个，alias出现的时候认为是针对alias的授权，否则认为是给topic的授权

**注意，只允许从access-manager获取token，并给该token授权。不允许使用非access-manager产生的token**

成功时额外返回参数：

参数名 | 类型 | 说明
------- | ------- | -------
token | string | 请求的grant为token层次的grant才返回token。

使用get请求：

```
http://localhost:8099/?appkey=581953df1a9bd33b8700e203&seckey=123456&method=yam_grant&w=1&r=1
```

使用post请求时的content：

```
{
"method":"yam_grant",
"appkey":"581953df1a9bd33b8700e203",
"seckey":"123456",
"w":"1",
"r":"0",
"topic" :"testtopic",
"token" :"testtoken"
}
```

#### yam_audit

检查`app`，`topic`或者`topic下的某个token`访问权限。

参数：

参数名 | 类型 | 说明
------- | ------- | -------
method | string | 必须使用"yam_audit"
topic | string | 可选参数。
alias | string | 可选参数。
token | string | 可选参数。
uid | string | 可选参数。

topic与token的处理同yam_grant。

额外返回参数：

参数名 | 类型 | 说明
------- | ------- | -------
w | int | 1：允许写， 0：禁止写
r | int | 1：允许读， 0：禁止读
p | int | 1：允许读presence， 0：禁止读presence

使用get请求：

```
http://localhost:8099/?appkey=581953df1a9bd33b8700e203&seckey=123456&method=yam_audit
```

使用post请求时的content：

```
{
"method":"yam_audit",
"appkey":"581953df1a9bd33b8700e203",
"seckey":"123456",
"topic" :"testtopic",
"token" :"testtoken"
}
```


