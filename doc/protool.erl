#! /usr/bin/env escript

main(_) ->
    {ok, _} = net_kernel:start(['mynode@127.0.0.1', longnames]),
    erlang:set_cookie(node(), eyam2),

    T1 = os:timestamp(),
    loop(10000),
    T2 = os:timestamp(),
    io:format("one loop for 1w request ~p ms ~n", [ timer:now_diff(T2,T1) ]),

    Self = self(),
    Pids = [spawn_link(fun()-> Self ! {self(),loop(100)} end) || _X <- lists:seq(1, 100)],
    [receive{Pid, ok} -> ok end || Pid <-Pids],

    T3 = os:timestamp(),

    io:format("100 loop for 100 request ~p ms ~n", [ timer:now_diff(T3, T2) ]).


loop(I) ->
    if I =:= 0-> ok ;
       true ->
            rpc:call('eyam2@127.0.0.1',eyam2_remote, check_permission, [<<"58072d1fd69873332db470a6">>, <<"test">>, <<"f61bfea56e4c55bda41e76b05878aa15">>, <<"r">>]),
            loop(I - 1)
    end.
