## eyam design

基本功能：

- 提供restful接口供用户开关YAM功能。
- 提供restful接口供用户根据app，topic，或者token进行权限管理。
- 提供rpc接口供其他模块校验某个token是否可以读／写某个app下的某个topic。

具体接口情参考相关文档
	
RESTful API : [RESTfulAPI.md](RESTfulAPI.md)

RPC API [rpc.md](rpc.md)


一个APP的YAM包括以下设置：
- YAM是否开启。
- 某个APP的读写权限是否设置。
- 某个topic的读写权限是否设置。
- 某个topic下某个用户的读写权限是否设置。

权限控制包括 以下角色：
- 客户的客户端
- 客户的服务端
- YAM
- yunba内依赖YAM的模块

权限控制工作流程应该如下：
a) 客户从服务端或者yunba的protal使能YAM。
b) 客户从他们的服务端掉用restful接口对app，topic，token进行读写授权。
c) 客户的客户端发送请求到yunba的服务。
d) yunba服务找YAM确认权限，根据YAM的返回决定是否允许该请求。

由于YAM的接口都需要seckey，所以这些请求不能由客户端发起。（improve:如果一定要客户端修改，发放admin权限）
	
### YAM 存储结构设计
YAM 只使用了redis作为存储。存储内容以及其结构如下：

key | value | note
------- | ------- | -------
A_(appid) | 0~3 | 某个appid的读写权限
C_(appid)_(topic) | 0～3 |某个appid下的某个topic的读写权限
U_(appid)_(topic)_(token) |0~3 |某个appid下，某个token对该topic的读写权限
I_(appid)_(topic)_(UID) |0~3 |某个appid下，某个uid对该topic的读写权限
K_(token)|1|某个token是否存在

读写权限相关说明：

Dec | Bin | note
------- | ------- | -------
0|00|r:0, w:0,读写都不可以
1|01|r:0, w:1,可写不可读
2|10|r:1, w:0,可读不可写
3|11|r:1, w:1，可读可写

YAM使能标识在redis内存储为无expire的。
其他权限的存储根据用户请求的ttl设置expire时间，单位为秒。如果ttl设置为0认为无expire。

### 权限判定逻辑
对于用户的客户端的某个请求，YAM的判定逻辑如下：

- 如果没有使能YAM，则直接返回true，否则进入下一步
- 如果设置了APP层级的权限，且该权限为允许该动作，则返回true，否则进入下一步
- 如果设置了topic层级的权限，且该权限为允许该动作，则返回true，否则进入下一步
- 如果设置token层级的权限，且该权限为允许该动作，则返回true，否则返回false。

### YAM模块即成相关

理论来说，每个服务如果需要准确无误的权限管理，应该是每次请求都进行一次数据校验的。但是这样YAM会有比较大的压力，一旦YAM不可用，集成了YAM的服务也不可用了。所以最好能允许客户端进行一定的缓存。具体key怎么设计可能每个客户端不一样，比较简单的是直接appid_topic_token对应一个read write权限，关键是多久更新：

- 固定时间更新。
- 让用户设定缓存时间，允许权限更新后多久生效。
- 根据ttl进行缓存。
- 根据用户设置，按照一定比例的ttl进行缓存，100%，50%，25%，0%。对于权限变更很不频繁的，可以选在100%ttl缓存，对于权限控制很敏感的，可以选择0%。

不论采取那种方案，缓存了就会存在权限管理出错的情况，最严格的控制还是直接找YAM询问，缓存只能是一种折中。但是缓存应该是能很大程度提升性能的，因为正常来说，一个APP是不会让自己的用户无授权直接发消息的，所以YAM的返回大多数为true，如果有大量缓存不命中，YAM返回false，那可能这个APP被hack了。

